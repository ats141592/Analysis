import astroquery.ipac.ned
import subprocess
import xspec




class definition_file:
    def __init__(self):
        self.bandpass           = ""
        self.column_density     = ""
        self.declination        = ""
        self.flux               = ""
        self.fout               = "0002.dat"
        self.object_name        = "NGC 3783"
        self.object             = []
        self.right_ascension    = ""
        self.specfile           = "0002.qdp"
        self.specformat         = ""
        self.specunits          = ""
        self.spectrum           = "user"

    
    def input_deffile(self):
        self.object             = astroquery.ipac.ned.Ned.query_object(object_name=self.object_name)
        self.bandpass           = "0.000-0.000"
        self.column_density     = "0.000"
        self.declination        = str(self.object["DEC"][0])
        self.flux               = "0.000, 0.000"
        self.right_ascension    = str(self.object["RA"][0])
        self.specformat         = "2"
        self.specunits          = "2"


    def output_deffile(self):
        with open(self.fout, mode="w") as fout:
            fout.write(self.right_ascension + ", ")
            fout.write(self.declination + ", ")
            fout.write(self.column_density + ", ")
            fout.write(self.spectrum + ", ")
            fout.write(self.flux + ", ")
            fout.write(self.bandpass + ", ")
            fout.write(self.specfile + ", ")
            fout.write(self.specunits + ", ")
            fout.write(self.specformat)

    
    def run_heasim(self):
        command = ["heasim"]
        command.append("mission=xrism")
        command.append("instrume=resolve")
        command.append("rapoint={0:s}".format(self.right_ascension))
        command.append("decpoint={0:s}".format(self.declination))
        command.append("roll=0.000")
        command.append("exposure=320000")
        command.append("insrcdeffile=0002.dat")
        command.append("outfile=0002.fits")
        command.append("psffile=/Users/tanimoto/software/heasoft/heasoft-6.34/heasim/heasimfiles/xrism/resolve/psf/eef_from_sxs_psfimage_20140618.fits")
        command.append("vigfile=/Users/tanimoto/software/heasoft/heasoft-6.34/heasim/heasimfiles/xrism/resolve/vignette/SXT_VIG_140618.txt")
        command.append("rmffile=/Users/tanimoto/software/heasoft/heasoft-6.34/heasim/heasimfiles/xrism/resolve/response/resolve_h5ev_2019a.rmf")
        command.append("arffile=/Users/tanimoto/software/heasoft/heasoft-6.34/heasim/heasimfiles/xrism/resolve/response/resolve_pnt_heasim_ND_withGV_20190701.arf")
        command.append("intbackfile=/Users/tanimoto/software/heasoft/heasoft-6.34/heasim/heasimfiles/xrism/resolve/background/resolve_h5ev_2019a_rslnxb.pha")
        command.append("flagsubex=no")
        command.append("seed=1")
        command.append("clobber=yes")
        subprocess.run(command, bufsize=1024)


    def save_model(self):
        subprocess.run("rm {0:s}".format(self.specfile), shell=True)
        xspec.Xset.restore("0001.xcm")
        xspec.AllData.clear()
        xspec.Spectrum.energies = [0.100, 10.00, 10001]
        xspec.Plot.addCommand("wdata {0:s}".format(self.specfile))
        xspec.Plot("model")


if __name__=="__main__":
    deffile = definition_file()
    deffile.save_model()
    deffile.input_deffile()
    deffile.output_deffile()
    deffile.run_heasim()